/*
	Aggregation Pipeline Stages

	Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage.

	$match - is used to match or get documents which satisfies the condition
	//syntax: {$match: {field:<value>}}
	
	$group - allows us to group together documents and create an analysis out of the grouped documents

	_id - in the group stage, essentially associates an id to our results. and also determines the number of groups


	$sum is used to add or total the values of the given field

	*/

db.fruits.aggregate([

	// looked for and got all fruits that are onSale
    {$match: {onSale:true}}, //apple,banana,mango,kiwi,dragon fruit
    
    /*
		apple
		supplier: Red Farms Inc

		banana
		supplier: Yellow Farms

		mango
		supplier: Yellow Farms

		kiwi
		supplier: Green Farming

		dragon fruit
		supplier: Red Farms Inc


		group 1
		_id: Red Farms Inc
		apple, dragon fruit

		group 2
		_id: Yellow Farms
		banana, mango

		group 3
		_id: Green Farming
		kiwi
    */

    {$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

// if the _id's value is definite or given, $group will only create one group
{$match: {onSale:true}},
{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

// if the _id's value is definite or given, $group will only create one group
{$match: {onSale:true}},
{$group: {_id:"AllFruits",totalStocks:{$sum:"$stocks"}}}
])

// $avg is an operator used in $group stage
// $avg gets the average of the numerical value of the indicated fields in grouped documents

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier", avgStock:{$avg:"$stocks"}}}
])

// $max is an operator used in $group stage
// $max - will allow us to get the highest value out of all the values in the given field per group


// highest number of stock among all items on sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id: "highestStockOnSale", maxStock: {$max: "$stocks"}}}
])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null, maxPrice:{$max: "$price"}}}
])

// $min is an operator used in $group stage
// $mix - gets the lowest value out of all the values in the given field per group

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"lowestStockOnSale", minStock:{$min: "$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"lowestPriceOnSale", minPrice:{$min: "$price"}}}

])
// get lowest stock of item's price less than 50
db.fruits.aggregate([

	{$match:{price:{$lt: 50}}},
	{$group:{_id:"lowestStock", minStock:{$min: "$stocks"}}}

])

// Other Stages

// $count - a stage added after $match stage to count all items that matches our criteria

// Count all items on Sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$count: "itemsOnSale"}

])

// Count all items whose price is less than 50
db.fruits.aggregate([

	{$match: {price:{$lt: 50}}},
	{$count: "itemsPriceLessThan50"}

])

// Number of items with stocks less than 20
db.fruits.aggregate([

	{$match: {stocks:{$lt: 20}}},
	{$count: "forRestock"}

])

// $out - save/output the results in a new collection
// note: this will overwrite the collection if it already exists

db.fruits.aggregate([
	{$match: {onSale:true}},
	{$group: {_id:"$supplier",totalStocks:{$sum:$stocks}}},
	{$out: "stocksPerSupplier"}
])