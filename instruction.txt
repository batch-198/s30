Mini Activity:

db.fruits.insertMany([
    {
      "name": "Apple",
      "supplier": "Red Farms Inc.",
      "stocks": 20,
      "price": 40,
      "onSale": true
    },
    {
      "name": "Banana",
      "supplier": "Yellow Farms",
      "stocks": 15,
      "price": 20,
      "onSale": true
    },
    {
      "name": "Kiwi",
      "supplier": "Green Farming and Canning",
      "stocks": 25,
      "price": 50,
      "onSale": true
    },
    {
      "name": "Mango",
      "supplier": "Yellow Farm",
      "stocks": 10,
      "price": 60,
      "onSale": true
    },
    {
      "name": "Dragon Fruit",
      "supplier": "Red Farms Inc.",
      "stocks": 10,
      "price": 60,
      "onSale": true
    }
])


Aggregate - luster of things that have come or have been brought together

Aggregations Pipelines
- a framework for data aggregation
- consists of stages and transforms the document as they pass through each stage

