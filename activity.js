
// COUNT OF Yellow Farm & Price $lt 50
db.fruits.aggregate([

	{$match:{$and: [{supplier:"Yellow Farms"},{price: {$lt:50}}]}},
	{$count: "YlwFrmStockPricedLessThan50"}
])

// COUNT OF all items with price less than 30
db.fruits.aggregate([

	{$match:{price: {$lt:30}}},
	{$count: "itemsPricedLessThan30"}
])

// AVG price of fruits by Yellow Farms
db.fruits.aggregate([

	{$match:{supplier: "Yellow Farms"}},
	{$group: {_id:"Yellow Farms", avePrice:{$avg: "$price"}}}
])

// MAX price of fruits by Red Farms Inc.
db.fruits.aggregate([

	{$match:{supplier: "Red Farms Inc."}},
	{$group: {_id:"Red Farms Inc.", maxPrice:{$max: "$price"}}}
])

// MIN price of fruits by Red Farms Inc.
db.fruits.aggregate([

	{$match:{supplier: "Red Farms Inc."}},
	{$group: {_id:"Red Farms Inc.", minPrice:{$min: "$price"}}}
])
